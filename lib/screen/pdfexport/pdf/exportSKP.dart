import 'dart:io';
import 'dart:typed_data';
import 'package:path_provider/path_provider.dart';
import 'package:pdf/pdf.dart';
import 'package:pdf/widgets.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:printing/printing.dart';

Future<Uint8List> sKP({
  required PdfPageFormat format,
  required String tanggalSurat,
  required String nomorsurat1,
  required String nomorsurat2,
  required String yangberttd,
  required String namaterang,
  required String jabatan,
  required String nip,
  required String kepada,
  required String nomorsuratpengirim,
  required String tanggalsuratpengirim,
  required String nama,
  required String prodi,
  required String nim,
  required String judul,
  required String tempat,
  required String tanggal,
  required String tembusan,
}) async {
  final pdf = Document();
  final images1 = MemoryImage(
      (await rootBundle.load('assets/image1.jpg')).buffer.asUint8List());
  final images2 = MemoryImage(
      (await rootBundle.load('assets/image2.jpg')).buffer.asUint8List());
  final ttf = await fontFromAssetBundle('assets/font-normal.ttf');
  final ttfb = await fontFromAssetBundle('assets/font-bold.ttf');

  // print(output);

  pdf.addPage(
    Page(
      build: (context) {
        return Column(
          children: [
            Row(
              children: [
                SizedBox(
                  height: 75,
                  width: 75,
                  child: Image(images1),
                ),
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        "PEMERINTAHAN KOTA KEDIRI",
                        style: TextStyle(
                          fontSize: 13,
                          color: PdfColors.black,
                          // font: ttf,
                          fontWeight: FontWeight.normal,
                          letterSpacing: 1,
                        ),
                      ),
                      Text(
                        "DINAS KESEHATAN",
                        style: TextStyle(
                          fontSize: 26,
                          color: PdfColors.black,
                          fontWeight: FontWeight.bold,
                          letterSpacing: 1,
                        ),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Text(
                        "Jalan Kartini No.07 Telp/Fax. (0354) 682001/671473",
                        style: TextStyle(
                          fontSize: 13,
                          color: PdfColors.black,
                          fontWeight: FontWeight.normal,
                        ),
                      ),
                      Text(
                        "Email : dinkeskotakediri@telkom.net.id",
                        style: TextStyle(
                          fontSize: 13,
                          color: PdfColors.black,
                          fontWeight: FontWeight.normal,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 10),
              child: Column(
                children: [
                  Container(
                    height: 1,
                    color: PdfColors.black,
                  ),
                  SizedBox(height: 1),
                  Container(
                    height: 1,
                    color: PdfColors.black,
                  ),
                ],
              ),
            ),
            Row(
              children: [
                SizedBox(
                  width: 300,
                ),
                Expanded(
                  child: Text(
                    "Kediri, $tanggalSurat",
                    style: TextStyle(
                        fontSize: 12,
                        color: PdfColors.black,
                        fontWeight: FontWeight.normal,
                        font: ttf),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 10,
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  width: 300,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          SizedBox(
                            width: 60,
                            child: Text(
                              "Nomor",
                              style: TextStyle(
                                fontSize: 12,
                                color: PdfColors.black,
                                fontWeight: FontWeight.normal,
                                font: ttf,
                              ),
                            ),
                          ),
                          Text(
                            ": $nomorsurat1     $nomorsurat2",
                            style: TextStyle(
                              fontSize: 12,
                              color: PdfColors.black,
                              fontWeight: FontWeight.normal,
                              font: ttf,
                            ),
                          ),
                        ],
                      ),
                      Row(
                        children: [
                          SizedBox(
                            width: 60,
                            child: Text(
                              "Sifat",
                              style: TextStyle(
                                fontSize: 12,
                                color: PdfColors.black,
                                fontWeight: FontWeight.normal,
                                font: ttf,
                              ),
                            ),
                          ),
                          Text(
                            ": Penting",
                            style: TextStyle(
                              fontSize: 12,
                              color: PdfColors.black,
                              fontWeight: FontWeight.normal,
                              font: ttf,
                            ),
                          ),
                        ],
                      ),
                      Row(
                        children: [
                          SizedBox(
                            width: 60,
                            child: Text(
                              "Lampiran",
                              style: TextStyle(
                                fontSize: 12,
                                color: PdfColors.black,
                                fontWeight: FontWeight.normal,
                                font: ttf,
                              ),
                            ),
                          ),
                          Text(
                            ": -",
                            style: TextStyle(
                              fontSize: 12,
                              color: PdfColors.black,
                              fontWeight: FontWeight.normal,
                              font: ttf,
                            ),
                          ),
                        ],
                      ),
                      Row(
                        children: [
                          SizedBox(
                            width: 60,
                            child: Text(
                              "Perihal",
                              style: TextStyle(
                                fontSize: 12,
                                color: PdfColors.black,
                                fontWeight: FontWeight.normal,
                                font: ttf,
                              ),
                            ),
                          ),
                          Text(
                            ":",
                            style: TextStyle(
                              fontSize: 12,
                              color: PdfColors.black,
                              fontWeight: FontWeight.normal,
                              font: ttf,
                            ),
                          ),
                          Text(
                            " Surat Keterangan Persetujuan",
                            style: TextStyle(
                              decoration: TextDecoration.underline,
                              fontSize: 12,
                              color: PdfColors.black,
                              fontWeight: FontWeight.bold,
                              font: ttfb,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(
                        "K e p a d a :",
                        style: TextStyle(
                          fontSize: 12,
                          color: PdfColors.black,
                          fontWeight: FontWeight.normal,
                          font: ttf,
                        ),
                      ),
                      Text(
                        "Yth. Sdr $kepada",
                        style: TextStyle(
                          fontSize: 12,
                          color: PdfColors.black,
                          fontWeight: FontWeight.normal,
                          font: ttf,
                        ),
                      ),
                      // Text(
                      //   univerasitas,
                      //   style: Theme.of(context).header3.copyWith(
                      //         fontSize: 9,
                      //         color: PdfColors.black,
                      //         fontWeight: FontWeight.normal,
                      //         letterSpacing: 1,
                      //       ),
                      // ),
                    ],
                  ),
                ),
              ],
            ),
            Row(
              children: [
                SizedBox(
                  width: 300,
                ),
                Expanded(
                  child: Padding(
                    padding: EdgeInsets.only(left: 20),
                    child: Text(
                      "Di -",
                      style: TextStyle(
                        fontSize: 12,
                        color: PdfColors.black,
                        fontWeight: FontWeight.normal,
                        font: ttf,
                      ),
                    ),
                  ),
                ),
              ],
            ),
            Row(
              children: [
                SizedBox(
                  width: 300,
                ),
                Expanded(
                  child: Padding(
                    padding: EdgeInsets.only(left: 30),
                    child: Text(
                      "TEMPAT",
                      style: TextStyle(
                        fontSize: 12,
                        decoration: TextDecoration.underline,
                        color: PdfColors.black,
                        fontWeight: FontWeight.bold,
                        font: ttfb,
                      ),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 20,
            ),
            Row(
              // crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                SizedBox(
                  width: 65,
                ),
                Expanded(
                  child: Text(
                    "\t \t \t \t \t Berdasarkan surat Saudara Nomor: $nomorsuratpengirim tanggal $tanggalsuratpengirim perihal seperti pada pokok surat, maka bersama ini diberitahukan bahwa pada prinsipnya kami tidak keberatan atas pelaksanaan kegiatan ilmiah bidang kesehatan dalam wilayah yang dilakukan oleh : ",
                    textAlign: TextAlign.justify,
                    style: TextStyle(
                      fontSize: 12,
                      color: PdfColors.black,
                      fontWeight: FontWeight.normal,
                      font: ttf,
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 10,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    SizedBox(width: 65),
                    SizedBox(
                      width: 55,
                      child: Text(
                        'Nama',
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          fontSize: 12,
                          color: PdfColors.black,
                          fontWeight: FontWeight.normal,
                          font: ttf,
                        ),
                      ),
                    ),
                    Text(
                      ': ',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 12,
                        color: PdfColors.black,
                        fontWeight: FontWeight.normal,
                        font: ttf,
                      ),
                    ),
                    Text(
                      nama,
                      textAlign: TextAlign.left,
                      style: TextStyle(
                        fontSize: 12,
                        color: PdfColors.black,
                        fontWeight: FontWeight.normal,
                        font: ttf,
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 3),
                Row(
                  children: [
                    SizedBox(width: 65),
                    SizedBox(
                      width: 55,
                      child: Text(
                        'Prodi',
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          fontSize: 12,
                          color: PdfColors.black,
                          fontWeight: FontWeight.normal,
                          font: ttf,
                        ),
                      ),
                    ),
                    Text(
                      ': ',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 12,
                        color: PdfColors.black,
                        fontWeight: FontWeight.normal,
                        font: ttf,
                      ),
                    ),
                    Text(
                      prodi,
                      textAlign: TextAlign.left,
                      style: TextStyle(
                        fontSize: 12,
                        color: PdfColors.black,
                        fontWeight: FontWeight.normal,
                        font: ttf,
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 3),
                Row(
                  children: [
                    SizedBox(width: 65),
                    SizedBox(
                      width: 55,
                      child: Text(
                        'NIM',
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          fontSize: 12,
                          color: PdfColors.black,
                          fontWeight: FontWeight.normal,
                          font: ttf,
                        ),
                      ),
                    ),
                    Text(
                      ': ',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 12,
                        color: PdfColors.black,
                        fontWeight: FontWeight.normal,
                        font: ttf,
                      ),
                    ),
                    Text(
                      nim,
                      textAlign: TextAlign.left,
                      style: TextStyle(
                        fontSize: 12,
                        color: PdfColors.black,
                        fontWeight: FontWeight.normal,
                        font: ttf,
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 3),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(width: 65),
                    SizedBox(
                      width: 55,
                      child: Text(
                        'Judul',
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          fontSize: 12,
                          color: PdfColors.black,
                          fontWeight: FontWeight.normal,
                          font: ttf,
                        ),
                      ),
                    ),
                    Text(
                      ': ',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 12,
                        color: PdfColors.black,
                        fontWeight: FontWeight.normal,
                        font: ttf,
                      ),
                    ),
                    Expanded(
                      child: Text(
                        judul,
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          fontSize: 12,
                          color: PdfColors.black,
                          fontWeight: FontWeight.normal,
                          font: ttf,
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 3),
                Row(
                  children: [
                    SizedBox(width: 65),
                    SizedBox(
                      width: 55,
                      child: Text(
                        'Tempat',
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          fontSize: 12,
                          color: PdfColors.black,
                          fontWeight: FontWeight.normal,
                          font: ttf,
                        ),
                      ),
                    ),
                    Text(
                      ': ',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 12,
                        color: PdfColors.black,
                        fontWeight: FontWeight.normal,
                        font: ttf,
                      ),
                    ),
                    Text(
                      tempat,
                      textAlign: TextAlign.left,
                      style: TextStyle(
                        fontSize: 12,
                        color: PdfColors.black,
                        fontWeight: FontWeight.normal,
                        font: ttf,
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 3),
                Row(
                  children: [
                    SizedBox(width: 65),
                    SizedBox(
                      width: 55,
                      child: Text(
                        'Tanggal',
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          fontSize: 12,
                          color: PdfColors.black,
                          fontWeight: FontWeight.normal,
                          font: ttf,
                        ),
                      ),
                    ),
                    Text(
                      ': ',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 12,
                        color: PdfColors.black,
                        fontWeight: FontWeight.normal,
                        font: ttf,
                      ),
                    ),
                    Text(
                      tanggal,
                      textAlign: TextAlign.left,
                      style: TextStyle(
                        fontSize: 12,
                        color: PdfColors.black,
                        fontWeight: FontWeight.normal,
                        font: ttf,
                      ),
                    ),
                  ],
                ),
              ],
            ),
            SizedBox(
              height: 20,
            ),
            Row(
              // crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                SizedBox(
                  width: 60,
                ),
                Expanded(
                  child: Text(
                    "\t \t \t \t \t \t Demikian atas perhatian dan kerjasamanya kami sampaikan terimakasih.",
                    style: TextStyle(
                      fontSize: 12,
                      color: PdfColors.black,
                      fontWeight: FontWeight.normal,
                      font: ttf,
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 30,
            ),
            Row(
              // crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                SizedBox(
                  width: 200,
                ),
                Expanded(
                    child: Column(children: [
                  Text(
                    yangberttd,
                    style: TextStyle(
                      fontSize: 12,
                      color: PdfColors.black,
                      fontWeight: FontWeight.bold,
                      font: ttfb,
                    ),
                  ),
                  Text(
                    "KOTA KEDIRI",
                    style: TextStyle(
                      fontSize: 12,
                      color: PdfColors.black,
                      fontWeight: FontWeight.bold,
                      font: ttfb,
                    ),
                  ),
                ])),
              ],
            ),
            SizedBox(
              height: 60,
            ),
            Row(
              // crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                SizedBox(
                  width: 200,
                ),
                Expanded(
                    child: Column(children: [
                  Text(
                    namaterang,
                    style: TextStyle(
                      decoration: TextDecoration.underline,
                      fontSize: 12,
                      color: PdfColors.black,
                      fontWeight: FontWeight.bold,
                      font: ttfb,
                    ),
                  ),
                  Text(
                    jabatan,
                    style: TextStyle(
                      fontSize: 12,
                      color: PdfColors.black,
                      fontWeight: FontWeight.bold,
                      font: ttfb,
                    ),
                  ),
                  Text(
                    "NIP. $nip",
                    style: TextStyle(
                      fontSize: 12,
                      color: PdfColors.black,
                      fontWeight: FontWeight.bold,
                      font: ttfb,
                    ),
                  ),
                ])),
              ],
            ),
            SizedBox(
              height: 40,
            ),
            Column(
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Tembusan disampaikan kepada Yth. :",
                      style: TextStyle(
                          fontSize: 12,
                          decoration: TextDecoration.underline,
                          color: PdfColors.black,
                          fontWeight: FontWeight.normal,
                          font: ttf),
                    ),
                  ],
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "\t \t \t 1. ",
                      style: TextStyle(
                          fontSize: 12,
                          color: PdfColors.black,
                          fontWeight: FontWeight.normal,
                          font: ttf),
                    ),
                    Expanded(
                      child: Text(
                        "Sdr. Kepala $tembusan Kota Kediri",
                        style: TextStyle(
                            fontSize: 12,
                            color: PdfColors.black,
                            fontWeight: FontWeight.normal,
                            font: ttf),
                      ),
                    )
                  ],
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "\t \t \t 2. ",
                      style: TextStyle(
                          fontSize: 12,
                          color: PdfColors.black,
                          fontWeight: FontWeight.normal,
                          font: ttf),
                    ),
                    Expanded(
                      child: Text(
                        "Yang bersangkutan",
                        style: TextStyle(
                            fontSize: 12,
                            color: PdfColors.black,
                            fontWeight: FontWeight.normal,
                            font: ttf),
                      ),
                    )
                  ],
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "\t \t \t 3. ",
                      style: TextStyle(
                          fontSize: 12,
                          color: PdfColors.black,
                          fontWeight: FontWeight.normal,
                          font: ttf),
                    ),
                    Expanded(
                      child: Text(
                        "A r s i p. ",
                        style: TextStyle(
                            fontSize: 12,
                            color: PdfColors.black,
                            fontWeight: FontWeight.normal,
                            font: ttf),
                      ),
                    )
                  ],
                ),
              ],
            ),
          ],
          // ),
        );
      },
    ),
  );

  return pdf.save();
}

Widget PaddedText(
  final String text, {
  final TextAlign align = TextAlign.left,
}) =>
    Padding(
      padding: EdgeInsets.all(5),
      child: Text(
        text,
        textAlign: align,
      ),
    );

Widget PaddedTextCenter(
  final String text, {
  final TextAlign align = TextAlign.center,
}) =>
    Padding(
      padding: EdgeInsets.all(5),
      child: Text(
        text,
        textAlign: align,
      ),
    );
