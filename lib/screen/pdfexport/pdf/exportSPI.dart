import 'dart:io';
import 'dart:typed_data';
import 'package:path_provider/path_provider.dart';
import 'package:pdf/pdf.dart';
import 'package:pdf/widgets.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:printing/printing.dart';

Future<Uint8List> sPIPreview(
    {required String tanggalSurat,
    required String kepada,
    required String nomorsurat1,
    required String nomorsurat2,
    required String yangberttd,
    required String namaterang,
    required String jabatan,
    required String nip,
    required PdfPageFormat format,
    required String nomorsuratpengirim,
    required String tanggalsuratpengirim,
    required String universitas,
    required String prodi,
    required String fakultas,
    required String tanggalkeperluan,
    required String tanggalkeperluan2,
    required String tembusan}) async {
  final pdf = Document();
  final images1 = MemoryImage(
      (await rootBundle.load('assets/image1.jpg')).buffer.asUint8List());
  final images2 = MemoryImage(
      (await rootBundle.load('assets/image2.jpg')).buffer.asUint8List());
  final ttf = await fontFromAssetBundle('assets/font-normal.ttf');
  final ttfb = await fontFromAssetBundle('assets/font-bold.ttf');
  pdf.addPage(
    Page(
      build: (context) {
        return Column(
          children: [
            Row(
              children: [
                SizedBox(
                  height: 75,
                  width: 75,
                  child: Image(images1),
                ),
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        "PEMERINTAHAN KOTA KEDIRI",
                        style: TextStyle(
                          fontSize: 13,
                          color: PdfColors.black,
                          // font: ttf,
                          fontWeight: FontWeight.normal,
                          letterSpacing: 1,
                        ),
                      ),
                      Text(
                        "DINAS KESEHATAN",
                        style: TextStyle(
                          fontSize: 26,
                          color: PdfColors.black,
                          fontWeight: FontWeight.bold,
                          letterSpacing: 1,
                        ),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Text(
                        "Jalan Kartini No.07 Telp/Fax. (0354) 682001/671473",
                        style: TextStyle(
                          fontSize: 13,
                          color: PdfColors.black,
                          fontWeight: FontWeight.normal,
                        ),
                      ),
                      Text(
                        "Email : dinkeskotakediri@telkom.net.id",
                        style: TextStyle(
                          fontSize: 13,
                          color: PdfColors.black,
                          fontWeight: FontWeight.normal,
                        ),
                      ),
                    ],
                  ),
                ),
                // SizedBox(
                //   height: 75,
                //   width: 75,
                //   child: Image(images2),
                // ),
              ],
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 10),
              child: Column(
                children: [
                  Container(
                    height: 1,
                    color: PdfColors.black,
                  ),
                  SizedBox(height: 1),
                  Container(
                    height: 1,
                    color: PdfColors.black,
                  ),
                ],
              ),
            ),
            Row(
              children: [
                SizedBox(
                  width: 300,
                ),
                Expanded(
                  child: Text(
                    "Kediri $tanggalSurat",
                    style: TextStyle(
                        fontSize: 12,
                        color: PdfColors.black,
                        fontWeight: FontWeight.normal,
                        font: ttf),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 10,
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  width: 300,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          SizedBox(
                            width: 60,
                            child: Text(
                              "Nomor",
                              style: TextStyle(
                                  fontSize: 12,
                                  color: PdfColors.black,
                                  fontWeight: FontWeight.normal,
                                  font: ttf),
                            ),
                          ),
                          Text(
                            ": $nomorsurat1       $nomorsurat2",
                            style: TextStyle(
                                fontSize: 12,
                                color: PdfColors.black,
                                fontWeight: FontWeight.normal,
                                font: ttf),
                          ),
                        ],
                      ),
                      Row(
                        children: [
                          SizedBox(
                            width: 60,
                            child: Text(
                              "Sifat",
                              style: TextStyle(
                                  fontSize: 12,
                                  color: PdfColors.black,
                                  fontWeight: FontWeight.normal,
                                  font: ttf),
                            ),
                          ),
                          Text(
                            ": Penting",
                            style: TextStyle(
                                fontSize: 12,
                                color: PdfColors.black,
                                fontWeight: FontWeight.normal,
                                font: ttf),
                          ),
                        ],
                      ),
                      Row(
                        children: [
                          SizedBox(
                            width: 60,
                            child: Text(
                              "Lampiran",
                              style: TextStyle(
                                  fontSize: 12,
                                  color: PdfColors.black,
                                  fontWeight: FontWeight.normal,
                                  font: ttf),
                            ),
                          ),
                          Text(
                            ": -",
                            style: TextStyle(
                                fontSize: 12,
                                color: PdfColors.black,
                                fontWeight: FontWeight.normal,
                                font: ttf),
                          ),
                        ],
                      ),
                      Row(
                        children: [
                          SizedBox(
                            width: 60,
                            child: Text(
                              "Perihal",
                              style: TextStyle(
                                  fontSize: 12,
                                  color: PdfColors.black,
                                  fontWeight: FontWeight.normal,
                                  font: ttf),
                            ),
                          ),
                          Text(
                            ":",
                            style: TextStyle(
                                fontSize: 12,
                                color: PdfColors.black,
                                fontWeight: FontWeight.normal,
                                font: ttf),
                          ),
                          Text(
                            " Permohonan Ijin PKl",
                            style: TextStyle(
                                decoration: TextDecoration.underline,
                                fontSize: 12,
                                color: PdfColors.black,
                                fontWeight: FontWeight.bold,
                                font: ttfb),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(
                        "K e p a d a :",
                        style: TextStyle(
                            fontSize: 12,
                            color: PdfColors.black,
                            fontWeight: FontWeight.normal,
                            font: ttf),
                      ),
                      Text(
                        "Yth. Sdr. $kepada ",
                        style: TextStyle(
                            fontSize: 12,
                            color: PdfColors.black,
                            fontWeight: FontWeight.normal,
                            font: ttf),
                      ),
                      Text(
                        universitas,
                        style: TextStyle(
                            fontSize: 12,
                            color: PdfColors.black,
                            fontWeight: FontWeight.normal,
                            font: ttf),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            Row(
              children: [
                SizedBox(
                  width: 300,
                ),
                Expanded(
                  child: Padding(
                    padding: EdgeInsets.only(left: 20),
                    child: Text(
                      "Di -",
                      style: Theme.of(context).header3.copyWith(
                            fontSize: 9,
                            color: PdfColors.black,
                            fontWeight: FontWeight.normal,
                            letterSpacing: 1,
                          ),
                    ),
                  ),
                ),
              ],
            ),
            Row(
              children: [
                SizedBox(
                  width: 300,
                ),
                Expanded(
                  child: Padding(
                    padding: EdgeInsets.only(left: 30),
                    child: Text(
                      "TEMPAT",
                      style: TextStyle(
                          fontSize: 12,
                          color: PdfColors.black,
                          decoration: TextDecoration.underline,
                          fontWeight: FontWeight.bold,
                          font: ttfb),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 10,
            ),
            Row(
              // crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                SizedBox(
                  width: 65,
                ),
                Expanded(
                  child: Text(
                    "\t \t \t \t \t Menunjuk surat Saudara Nomor : $nomorsuratpengirim tanggal $tanggalsuratpengirim  perihal seperti pada pokok surat, bersama ini kami beritahukan bahwa pada prinsipnya kami tidak keberatan atas pelaksanaan Praktek Kerja Lapangan (PKL)  Oleh Mahasiswa $universitas Progam Studi $prodi Fakultas $fakultas, di Wilayah Dinas Kesehatan Kota Kediri Pada Tanggal $tanggalkeperluan s/d $tanggalkeperluan2",
                    textAlign: TextAlign.justify,
                    style: TextStyle(
                        fontSize: 12,
                        color: PdfColors.black,
                        fontWeight: FontWeight.normal,
                        font: ttf),
                  ),
                ),
              ],
            ),
            Row(
              // crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                SizedBox(
                  width: 65,
                ),
                Expanded(
                  child: Column(
                    children: [
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "1. ",
                            style: TextStyle(
                                fontSize: 12,
                                color: PdfColors.black,
                                fontWeight: FontWeight.normal,
                                font: ttf),
                          ),
                          Expanded(
                            child: Text(
                              "Sebelum Melaksanakan Praktek Melakukan Koordinasi dengan Dinas Kesehatan (Sub Koordinator  Penyusunan Program Dan Keuangan).",
                              textAlign: TextAlign.justify,
                              style: TextStyle(
                                  fontSize: 12,
                                  color: PdfColors.black,
                                  fontWeight: FontWeight.normal,
                                  font: ttf),
                            ),
                          )
                        ],
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "2. ",
                            style: TextStyle(
                                fontSize: 12,
                                color: PdfColors.black,
                                fontWeight: FontWeight.normal,
                                font: ttf),
                          ),
                          Expanded(
                            child: Text(
                              "Selama melaksanakan kegiatan penelitan berkewajiban mentaati segala ketentuan dan tata tertib yang berlaku.",
                              textAlign: TextAlign.justify,
                              style: TextStyle(
                                  fontSize: 12,
                                  color: PdfColors.black,
                                  fontWeight: FontWeight.normal,
                                  font: ttf),
                            ),
                          )
                        ],
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "3. ",
                            style: TextStyle(
                                fontSize: 12,
                                color: PdfColors.black,
                                fontWeight: FontWeight.normal,
                                font: ttf),
                          ),
                          Expanded(
                            child: Text(
                              "Melaporkan hasil praktek.",
                              style: TextStyle(
                                  fontSize: 12,
                                  color: PdfColors.black,
                                  fontWeight: FontWeight.normal,
                                  font: ttf),
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 10,
            ),
            Row(
              // crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                SizedBox(
                  width: 65,
                ),
                Expanded(
                  child: Text(
                    "\t \t \t \t \t Demikian atas perhatian dan kerjasamanya kami sampaikan terimakasih.",
                    style: TextStyle(
                        fontSize: 12,
                        color: PdfColors.black,
                        fontWeight: FontWeight.normal,
                        font: ttf),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 30,
            ),
            Row(
              // crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                SizedBox(
                  width: 200,
                ),
                Expanded(
                    child: Column(children: [
                  Text(
                    yangberttd,
                    style: TextStyle(
                        fontSize: 12,
                        color: PdfColors.black,
                        fontWeight: FontWeight.bold,
                        font: ttfb),
                  ),
                  Text(
                    "KOTA KEDIRI",
                    style: TextStyle(
                        fontSize: 12,
                        color: PdfColors.black,
                        fontWeight: FontWeight.bold,
                        font: ttfb),
                  ),
                ])),
              ],
            ),
            SizedBox(
              height: 60,
            ),
            Row(
              // crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                SizedBox(
                  width: 200,
                ),
                Expanded(
                    child: Column(children: [
                  Text(
                    namaterang,
                    style: TextStyle(
                        decoration: TextDecoration.underline,
                        fontSize: 12,
                        color: PdfColors.black,
                        fontWeight: FontWeight.bold,
                        font: ttfb),
                  ),
                  Text(
                    jabatan,
                    style: TextStyle(
                        fontSize: 12,
                        color: PdfColors.black,
                        fontWeight: FontWeight.bold,
                        font: ttfb),
                  ),
                  Text(
                    "NIP. $nip",
                    style: TextStyle(
                        fontSize: 12,
                        color: PdfColors.black,
                        fontWeight: FontWeight.bold,
                        font: ttfb),
                  ),
                ])),
              ],
            ),
            SizedBox(
              height: 40,
            ),
            Column(
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Tembusan disampaikan kepada Yth. :",
                      style: TextStyle(
                          fontSize: 12,
                          decoration: TextDecoration.underline,
                          color: PdfColors.black,
                          fontWeight: FontWeight.normal,
                          font: ttf),
                    ),
                  ],
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "\t \t \t 1. ",
                      style: TextStyle(
                          fontSize: 12,
                          color: PdfColors.black,
                          fontWeight: FontWeight.normal,
                          font: ttf),
                    ),
                    Expanded(
                      child: Text(
                        "Sdr. Sub $tembusan Kota Kediri",
                        style: TextStyle(
                            fontSize: 12,
                            color: PdfColors.black,
                            fontWeight: FontWeight.normal,
                            font: ttf),
                      ),
                    )
                  ],
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "\t \t \t 2. ",
                      style: TextStyle(
                          fontSize: 12,
                          color: PdfColors.black,
                          fontWeight: FontWeight.normal,
                          font: ttf),
                    ),
                    Expanded(
                      child: Text(
                        "A r s i p. ",
                        style: TextStyle(
                            fontSize: 12,
                            color: PdfColors.black,
                            fontWeight: FontWeight.normal,
                            font: ttf),
                      ),
                    )
                  ],
                ),
              ],
            ),
          ],
          // ),
        );
      },
    ),
  );
  return pdf.save();
}

class pdfApi {
  static Future<Future<File>> sPI(
      {required String tanggalSurat,
      required String kepada,
      required String nomorsurat1,
      required String nomorsurat2,
      required String yangberttd,
      required String namaterang,
      required String jabatan,
      required String nip,
      required String nomorsuratpengirim,
      required String tanggalsuratpengirim,
      required String universitas,
      required String prodi,
      required String fakultas,
      required String tanggalkeperluan,
      required String tembusan}) async {
    final pdf = Document();
    final images1 = MemoryImage(
        (await rootBundle.load('assets/image1.jpg')).buffer.asUint8List());
    final images2 = MemoryImage(
        (await rootBundle.load('assets/image2.jpg')).buffer.asUint8List());
    final ttf = await fontFromAssetBundle('assets/font-normal.ttf');
    final ttfb = await fontFromAssetBundle('assets/font-bold.ttf');
    pdf.addPage(
      Page(
        build: (context) {
          return Column(
            children: [
              Row(
                children: [
                  SizedBox(
                    height: 75,
                    width: 75,
                    child: Image(images1),
                  ),
                  Expanded(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          "PEMERINTAHAN KOTA KEDIRI",
                          style: TextStyle(
                            fontSize: 13,
                            color: PdfColors.black,
                            // font: ttf,
                            fontWeight: FontWeight.normal,
                            letterSpacing: 1,
                          ),
                        ),
                        Text(
                          "DINAS KESEHATAN",
                          style: TextStyle(
                            fontSize: 26,
                            color: PdfColors.black,
                            fontWeight: FontWeight.bold,
                            letterSpacing: 1,
                          ),
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Text(
                          "Jalan Kartini No.07 Telp/Fax. (0354) 682001/671473",
                          style: TextStyle(
                            fontSize: 13,
                            color: PdfColors.black,
                            fontWeight: FontWeight.normal,
                          ),
                        ),
                        Text(
                          "Email : dinkeskotakediri@telkom.net.id",
                          style: TextStyle(
                            fontSize: 13,
                            color: PdfColors.black,
                            fontWeight: FontWeight.normal,
                          ),
                        ),
                      ],
                    ),
                  ),
                  // SizedBox(
                  //   height: 75,
                  //   width: 75,
                  //   child: Image(images2),
                  // ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 10),
                child: Column(
                  children: [
                    Container(
                      height: 1,
                      color: PdfColors.black,
                    ),
                    SizedBox(height: 1),
                    Container(
                      height: 1,
                      color: PdfColors.black,
                    ),
                  ],
                ),
              ),
              Row(
                children: [
                  SizedBox(
                    width: 300,
                  ),
                  Expanded(
                    child: Text(
                      "Kediri $tanggalSurat",
                      style: TextStyle(
                          fontSize: 12,
                          color: PdfColors.black,
                          fontWeight: FontWeight.normal,
                          font: ttf),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    width: 300,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            SizedBox(
                              width: 60,
                              child: Text(
                                "Nomor",
                                style: TextStyle(
                                    fontSize: 12,
                                    color: PdfColors.black,
                                    fontWeight: FontWeight.normal,
                                    font: ttf),
                              ),
                            ),
                            Text(
                              ": $nomorsurat1       $nomorsurat2",
                              style: TextStyle(
                                  fontSize: 12,
                                  color: PdfColors.black,
                                  fontWeight: FontWeight.normal,
                                  font: ttf),
                            ),
                          ],
                        ),
                        Row(
                          children: [
                            SizedBox(
                              width: 60,
                              child: Text(
                                "Sifat",
                                style: TextStyle(
                                    fontSize: 12,
                                    color: PdfColors.black,
                                    fontWeight: FontWeight.normal,
                                    font: ttf),
                              ),
                            ),
                            Text(
                              ": Penting",
                              style: TextStyle(
                                  fontSize: 12,
                                  color: PdfColors.black,
                                  fontWeight: FontWeight.normal,
                                  font: ttf),
                            ),
                          ],
                        ),
                        Row(
                          children: [
                            SizedBox(
                              width: 60,
                              child: Text(
                                "Lampiran",
                                style: TextStyle(
                                    fontSize: 12,
                                    color: PdfColors.black,
                                    fontWeight: FontWeight.normal,
                                    font: ttf),
                              ),
                            ),
                            Text(
                              ": -",
                              style: TextStyle(
                                  fontSize: 12,
                                  color: PdfColors.black,
                                  fontWeight: FontWeight.normal,
                                  font: ttf),
                            ),
                          ],
                        ),
                        Row(
                          children: [
                            SizedBox(
                              width: 60,
                              child: Text(
                                "Perihal",
                                style: TextStyle(
                                    fontSize: 12,
                                    color: PdfColors.black,
                                    fontWeight: FontWeight.normal,
                                    font: ttf),
                              ),
                            ),
                            Text(
                              ":",
                              style: TextStyle(
                                  fontSize: 12,
                                  color: PdfColors.black,
                                  fontWeight: FontWeight.normal,
                                  font: ttf),
                            ),
                            Text(
                              " Permohonan Ijin PKl",
                              style: TextStyle(
                                  decoration: TextDecoration.underline,
                                  fontSize: 12,
                                  color: PdfColors.black,
                                  fontWeight: FontWeight.bold,
                                  font: ttfb),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text(
                          "K e p a d a :",
                          style: TextStyle(
                              fontSize: 12,
                              color: PdfColors.black,
                              fontWeight: FontWeight.normal,
                              font: ttf),
                        ),
                        Text(
                          "Yth. Sdr. $kepada ",
                          style: TextStyle(
                              fontSize: 12,
                              color: PdfColors.black,
                              fontWeight: FontWeight.normal,
                              font: ttf),
                        ),
                        Text(
                          universitas,
                          style: TextStyle(
                              fontSize: 12,
                              color: PdfColors.black,
                              fontWeight: FontWeight.normal,
                              font: ttf),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              Row(
                children: [
                  SizedBox(
                    width: 300,
                  ),
                  Expanded(
                    child: Padding(
                      padding: EdgeInsets.only(left: 20),
                      child: Text(
                        "Di -",
                        style: Theme.of(context).header3.copyWith(
                              fontSize: 9,
                              color: PdfColors.black,
                              fontWeight: FontWeight.normal,
                              letterSpacing: 1,
                            ),
                      ),
                    ),
                  ),
                ],
              ),
              Row(
                children: [
                  SizedBox(
                    width: 300,
                  ),
                  Expanded(
                    child: Padding(
                      padding: EdgeInsets.only(left: 30),
                      child: Text(
                        "TEMPAT",
                        style: TextStyle(
                            fontSize: 12,
                            color: PdfColors.black,
                            decoration: TextDecoration.underline,
                            fontWeight: FontWeight.bold,
                            font: ttfb),
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Row(
                // crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  SizedBox(
                    width: 65,
                  ),
                  Expanded(
                    child: Text(
                      "\t \t \t \t \t Menunjuk surat Saudara Nomor : $nomorsuratpengirim tanggal $tanggalsuratpengirim  perihal seperti pada pokok surat, bersama ini kami beritahukan bahwa pada prinsipnya kami tidak keberatan atas pelaksanaan Praktek Kerja Lapangan (PKL)  Oleh Mahasiswa $universitas Progam Studi $prodi Fakultas $fakultas Di Dinas Kesehatan Kota Kediri ,di Dinas Kesehatan Kota Kediri Pada Tanggal $tanggalkeperluan",
                      style: TextStyle(
                          fontSize: 12,
                          color: PdfColors.black,
                          fontWeight: FontWeight.normal,
                          font: ttf),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Row(
                // crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  SizedBox(
                    width: 90,
                  ),
                  Expanded(
                    child: Column(
                      children: [
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "1. ",
                              style: TextStyle(
                                  fontSize: 12,
                                  color: PdfColors.black,
                                  fontWeight: FontWeight.normal,
                                  font: ttf),
                            ),
                            Expanded(
                              child: Text(
                                "Sebelum Melaksanakan Praktek Melakukan Koordinasi dengan Dinas Kesehatan (Sub Koordinator  Penyusunan Program Dan Keuangan).",
                                style: TextStyle(
                                    fontSize: 12,
                                    color: PdfColors.black,
                                    fontWeight: FontWeight.normal,
                                    font: ttf),
                              ),
                            )
                          ],
                        ),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "2. ",
                              style: TextStyle(
                                  fontSize: 12,
                                  color: PdfColors.black,
                                  fontWeight: FontWeight.normal,
                                  font: ttf),
                            ),
                            Expanded(
                              child: Text(
                                "Selama melaksanakan kegiatan penelitan berkewajiban mentaati segala ketentuan dan tata tertib yang berlaku.",
                                style: TextStyle(
                                    fontSize: 12,
                                    color: PdfColors.black,
                                    fontWeight: FontWeight.normal,
                                    font: ttf),
                              ),
                            )
                          ],
                        ),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "3. ",
                              style: TextStyle(
                                  fontSize: 12,
                                  color: PdfColors.black,
                                  fontWeight: FontWeight.normal,
                                  font: ttf),
                            ),
                            Expanded(
                              child: Text(
                                "Melaporkan hasil praktek.",
                                style: TextStyle(
                                    fontSize: 12,
                                    color: PdfColors.black,
                                    fontWeight: FontWeight.normal,
                                    font: ttf),
                              ),
                            )
                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Row(
                // crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  SizedBox(
                    width: 65,
                  ),
                  Expanded(
                    child: Text(
                      "\t \t \t \t \t Demikian atas perhatian dan kerjasamanya kami sampaikan terimakasih.",
                      style: TextStyle(
                          fontSize: 12,
                          color: PdfColors.black,
                          fontWeight: FontWeight.normal,
                          font: ttf),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 30,
              ),
              Row(
                // crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  SizedBox(
                    width: 200,
                  ),
                  Expanded(
                      child: Column(children: [
                    Text(
                      yangberttd,
                      style: TextStyle(
                          fontSize: 12,
                          color: PdfColors.black,
                          fontWeight: FontWeight.bold,
                          font: ttfb),
                    ),
                    Text(
                      "KOTA KEDIRI",
                      style: TextStyle(
                          fontSize: 12,
                          color: PdfColors.black,
                          fontWeight: FontWeight.bold,
                          font: ttfb),
                    ),
                  ])),
                ],
              ),
              SizedBox(
                height: 60,
              ),
              Row(
                // crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  SizedBox(
                    width: 200,
                  ),
                  Expanded(
                      child: Column(children: [
                    Text(
                      namaterang,
                      style: TextStyle(
                          decoration: TextDecoration.underline,
                          fontSize: 12,
                          color: PdfColors.black,
                          fontWeight: FontWeight.bold,
                          font: ttfb),
                    ),
                    Text(
                      jabatan,
                      style: TextStyle(
                          fontSize: 12,
                          color: PdfColors.black,
                          fontWeight: FontWeight.bold,
                          font: ttfb),
                    ),
                    Text(
                      "NIP. $nip",
                      style: TextStyle(
                          fontSize: 12,
                          color: PdfColors.black,
                          fontWeight: FontWeight.bold,
                          font: ttfb),
                    ),
                  ])),
                ],
              ),
              SizedBox(
                height: 40,
              ),
              Column(
                children: [
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Tembusan disampaikan kepada Yth. :",
                        style: TextStyle(
                            fontSize: 12,
                            decoration: TextDecoration.underline,
                            color: PdfColors.black,
                            fontWeight: FontWeight.normal,
                            font: ttf),
                      ),
                    ],
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "\t \t \t 1. ",
                        style: TextStyle(
                            fontSize: 12,
                            color: PdfColors.black,
                            fontWeight: FontWeight.normal,
                            font: ttf),
                      ),
                      Expanded(
                        child: Text(
                          "Sdr. Sub $tembusan Kota Kediri",
                          style: TextStyle(
                              fontSize: 12,
                              color: PdfColors.black,
                              fontWeight: FontWeight.normal,
                              font: ttf),
                        ),
                      )
                    ],
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "\t \t \t 2. ",
                        style: TextStyle(
                            fontSize: 12,
                            color: PdfColors.black,
                            fontWeight: FontWeight.normal,
                            font: ttf),
                      ),
                      Expanded(
                        child: Text(
                          "A r s i p. ",
                          style: TextStyle(
                              fontSize: 12,
                              color: PdfColors.black,
                              fontWeight: FontWeight.normal,
                              font: ttf),
                        ),
                      )
                    ],
                  ),
                ],
              ),
            ],
            // ),
          );
        },
      ),
    );
    return savePdf(pdf: pdf);
  }

  static Future<File> savePdf({
    required Document pdf,
  }) async {
    final bytes = await pdf.save();

    // buat file kosong di direktori
    final dir = await getApplicationDocumentsDirectory();
    final file = File('${dir.path}/mydocument.pdf');

    // timpa file kosong dengan file pdf
    await file.writeAsBytes(bytes);

    return file;
  }
}

Widget PaddedText(
  final String text, {
  final TextAlign align = TextAlign.left,
}) =>
    Padding(
      padding: EdgeInsets.all(10),
      child: Text(
        text,
        textAlign: align,
      ),
    );
