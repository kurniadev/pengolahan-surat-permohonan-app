import 'package:flutter/material.dart';
import 'package:pdfly/screen/pdfexport/pdf/exportSKP.dart';
import 'package:pdfly/screen/pdfexport/pdf/exportSPI.dart';
import 'package:printing/printing.dart';

class PdfView2 extends StatefulWidget {
  const PdfView2({
    super.key,
    required this.tanggalSurat,
    required this.kepada,
    required this.nomorsuratpengirim,
    required this.tanggalsuratpengirim,
    required this.nama,
    required this.prodi,
    required this.nim,
    required this.judul,
    required this.tempat,
    required this.tanggal,
    required this.tembusan,
    required this.nomorsurat1,
    required this.nomorsurat2,
    required this.yangberttd,
    required this.namaterang,
    required this.jabatan,
    required this.nip,
  });

  final String tanggalSurat,
      kepada,
      nomorsuratpengirim,
      tanggalsuratpengirim,
      nama,
      prodi,
      nim,
      judul,
      tempat,
      tanggal,
      nomorsurat1,
      nomorsurat2,
      yangberttd,
      namaterang,
      jabatan,
      nip,
      tembusan;

  @override
  State<PdfView2> createState() => _PdfView2State();
}

class _PdfView2State extends State<PdfView2> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "Surat Keterangan Persetujuan",
          style: TextStyle(
            fontSize: 18,
            color: Colors.white,
            fontFamily: 'Poppins',
            fontWeight: FontWeight.w500,
            letterSpacing: 1,
          ),
        ),
        elevation: 0,
        backgroundColor: const Color(0xff276aa5),
        iconTheme: const IconThemeData(color: Colors.white),
      ),
      body: PdfPreview(
         canChangeOrientation: false,
        canChangePageFormat: false,
        canDebug: false,
        
        build: (format) => sKP(
          tanggalSurat: widget.tanggalSurat,
          kepada: widget.kepada,
          nomorsuratpengirim: widget.nomorsuratpengirim,
          tanggalsuratpengirim: widget.tanggalsuratpengirim,
          nama: widget.nama,
          prodi: widget.prodi,
          nim: widget.nim,
          judul: widget.judul,
          tempat: widget.tempat,
          tanggal: widget.tanggal,
          tembusan: widget.tembusan,
          format: format,
          jabatan: widget.jabatan,
          namaterang: widget.namaterang,
          nip: widget.nip,
          nomorsurat1: widget.nomorsurat1,
          nomorsurat2: widget.nomorsurat2,
          yangberttd: widget.yangberttd,
        ),
      ),
    );
  }
}
