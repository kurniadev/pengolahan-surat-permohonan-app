import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import 'package:pdfly/screen/pdfexport/pdf/exportSPI.dart';
import 'package:printing/printing.dart';

class PdfView1 extends StatefulWidget {
  const PdfView1({
    super.key,
    required this.tanggalSurat,
    required this.kepada,
    required this.nomorsuratpengirim,
    required this.tanggalsuratpengirim,
    required this.universitas,
    required this.prodi,
    required this.fakultas,
    required this.tanggalkeperluan,
    required this.tembusan,
    required this.nomorsurat1,
    required this.nomorsurat2,
    required this.yangberttd,
    required this.namaterang,
    required this.jabatan,
    required this.nip,
    required this.tanggalkeperluan2,
  });

  final String tanggalSurat,
      kepada,
      nomorsuratpengirim,
      tanggalsuratpengirim,
      universitas,
      prodi,
      fakultas,
      tanggalkeperluan,
      tanggalkeperluan2,
      tembusan,
      nomorsurat1,
      nomorsurat2,
      yangberttd,
      namaterang,
      jabatan,
      nip;

  @override
  State<PdfView1> createState() => _PdfView1State();
}

class _PdfView1State extends State<PdfView1> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "Surat Permohonan Ijin",
          style: TextStyle(
            fontSize: 18,
            color: Colors.white,
            fontFamily: 'Poppins',
            fontWeight: FontWeight.w500,
            letterSpacing: 1,
          ),
        ),
        elevation: 0,
        backgroundColor: const Color(0xff276aa5),
        iconTheme: const IconThemeData(color: Colors.white),
      ),
      body: PdfPreview(
        canChangeOrientation: false,
        canChangePageFormat: false,
        canDebug: false,
        build: (format) => sPIPreview(
          tanggalSurat: widget.tanggalSurat,
          kepada: widget.kepada,
          nomorsuratpengirim: widget.nomorsuratpengirim,
          tanggalsuratpengirim: widget.tanggalsuratpengirim,
          universitas: widget.universitas,
          prodi: widget.prodi,
          fakultas: widget.fakultas,
          tanggalkeperluan: widget.tanggalkeperluan,
          tembusan: widget.tembusan,
          format: format,
          jabatan: widget.jabatan,
          namaterang: widget.namaterang,
          nip: widget.nip,
          nomorsurat1: widget.nomorsurat1,
          nomorsurat2: widget.nomorsurat2,
          yangberttd: widget.yangberttd,
          tanggalkeperluan2: widget.tanggalkeperluan2,
        ),
      ),
    );
  }
}
