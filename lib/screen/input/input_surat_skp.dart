import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:intl/intl.dart';
import 'package:pdfly/screen/pdfexport/pdfView2.dart';

import '../../model/surat_skp_model.dart';

class InputSuratSKP extends StatefulWidget {
  const InputSuratSKP(
      {super.key,
      required this.nomorsurat1,
      required this.nomorsurat2,
      required this.yangberttd,
      required this.namaterang,
      required this.jabatan,
      required this.nip});

  final String nomorsurat1, nomorsurat2, yangberttd, namaterang, jabatan, nip;

  @override
  State<InputSuratSKP> createState() => _InputSuratSKPState();
}

class _InputSuratSKPState extends State<InputSuratSKP> {
  final storage = const FlutterSecureStorage();

  final List<SuratSkpModel> listSuratSkp = [];
  saveDataToStorage() async {
    final List<Object> tmp = [];
    for (var item in listSuratSkp) {
      tmp.add(item.toJson());
    }
    await storage.write(key: 'list-surat-skp', value: jsonEncode(tmp));
  }

  _getDataFromStorage() async {
    String? data = await storage.read(key: 'list-surat-skp');
    if (data != null) {
      final dataDecoded = jsonDecode(data);
      if (dataDecoded is List) {
        setState(() {
          listSuratSkp.clear();
          for (var item in dataDecoded) {
            listSuratSkp.add(SuratSkpModel.fromJson(item));
          }
        });
      }
    }
    print(json.encode(listSuratSkp));
  }

  @override
  void initState() {
    _getDataFromStorage();
    super.initState();
  }

  TextEditingController tanggalSurat = TextEditingController();
  TextEditingController kepada = TextEditingController();
  TextEditingController nomorsuratpengirim = TextEditingController();
  TextEditingController tanggalsuratpengirim = TextEditingController();
  TextEditingController nama = TextEditingController();
  TextEditingController prodi = TextEditingController();
  TextEditingController nim = TextEditingController();
  TextEditingController judul = TextEditingController();
  TextEditingController tempat = TextEditingController();
  TextEditingController tanggal = TextEditingController();
  TextEditingController tembusan = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: GestureDetector(
          onTap: () {
            _getDataFromStorage();
          },
          child: Text(
            "Surat Keterangan Persetujuan",
            style: TextStyle(
              fontSize: 18,
              color: Colors.white,
              fontFamily: 'Poppins',
              fontWeight: FontWeight.w500,
              letterSpacing: 1,
            ),
          ),
        ),
        elevation: 0,
        backgroundColor: const Color(0xff276aa5),
        iconTheme: const IconThemeData(color: Colors.white),
      ),
      body: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              child: ListView(
                children: [
                  const Text(
                    "Tanggal Surat",
                    style: TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.w600,
                      fontFamily: 'Poppins',
                      color: Color(0xff4B556B),
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Container(
                    height: 50,
                    decoration: BoxDecoration(
                        border:
                            Border.all(color: Color(0xff276aa5), width: 1.0),
                        borderRadius: BorderRadius.circular(10)),
                    child: Padding(
                      padding: const EdgeInsets.only(left: 10),
                      child: TextFormField(
                        controller: tanggalSurat,
                        readOnly: true,
                        decoration: const InputDecoration(
                          border: InputBorder.none,
                          suffixIcon: Icon(
                            Icons.calendar_month,
                          ),
                          hintText: 'dd-mm-yyyy',
                        ),
                        onTap: () async {
                          DateTime? pickedDate = await showDatePicker(
                              context: context,
                              initialDate: DateTime.now(),
                              firstDate: DateTime(2000),
                              lastDate: DateTime(2101));

                          if (pickedDate != null) {
                            // print(pickedDate);
                            String formattedDate =
                                DateFormat("yyyy-MM-dd").format(pickedDate);

                            setState(() {
                              var m = DateFormat('MM').format(pickedDate);
                              var d = DateFormat('dd')
                                  .format(pickedDate)
                                  .toString();
                              var Y = DateFormat('yyyy')
                                  .format(pickedDate)
                                  .toString();
                              var month = "";
                              switch (m) {
                                case '01':
                                  {
                                    month = "Januari";
                                  }
                                  break;
                                case '02':
                                  {
                                    month = "Februari";
                                  }
                                  break;
                                case '03':
                                  {
                                    month = "Maret";
                                  }
                                  break;
                                case '04':
                                  {
                                    month = "April";
                                  }
                                  break;
                                case '05':
                                  {
                                    month = "Mei";
                                  }
                                  break;
                                case '06':
                                  {
                                    month = "Juni";
                                  }
                                  break;
                                case '07':
                                  {
                                    month = "Juli";
                                  }
                                  break;
                                case '08':
                                  {
                                    month = "Agustus";
                                  }
                                  break;
                                case '09':
                                  {
                                    month = "September";
                                  }
                                  break;
                                case '10':
                                  {
                                    month = "Oktober";
                                  }
                                  break;
                                case '11':
                                  {
                                    month = "November";
                                  }
                                  break;
                                case '12':
                                  {
                                    month = "Desember";
                                  }
                                  break;
                              }
                              tanggalSurat.text = "$d $month $Y";
                            });
                          } else {
                            print("Date is not selected");
                          }
                        },
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Wajib Di isi';
                          }
                          return null;
                        },
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  const Text(
                    "Kepada",
                    style: TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.w600,
                      fontFamily: 'Poppins',
                      color: Color(0xff4B556B),
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Container(
                    decoration: BoxDecoration(
                        border:
                            Border.all(color: Color(0xff276aa5), width: 1.0),
                        borderRadius: BorderRadius.circular(10)),
                    child: Padding(
                      padding: const EdgeInsets.only(left: 10),
                      child: TextFormField(
                        controller: kepada,
                        decoration: const InputDecoration(
                          border: InputBorder.none,
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  const Text(
                    "Nomor Surat Pengirim",
                    style: TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.w600,
                      fontFamily: 'Poppins',
                      color: Color(0xff4B556B),
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Container(
                    decoration: BoxDecoration(
                        border:
                            Border.all(color: Color(0xff276aa5), width: 1.0),
                        borderRadius: BorderRadius.circular(10)),
                    child: Padding(
                      padding: const EdgeInsets.only(left: 10),
                      child: TextFormField(
                        controller: nomorsuratpengirim,
                        // keyboardType: TextInputType.number,
                        decoration: const InputDecoration(
                          border: InputBorder.none,
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  const Text(
                    "Tanggal Surat Pengirim",
                    style: TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.w600,
                      fontFamily: 'Poppins',
                      color: Color(0xff4B556B),
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Container(
                    height: 50,
                    decoration: BoxDecoration(
                        border:
                            Border.all(color: Color(0xff276aa5), width: 1.0),
                        borderRadius: BorderRadius.circular(10)),
                    child: Padding(
                      padding: const EdgeInsets.only(left: 10),
                      child: TextFormField(
                        controller: tanggalsuratpengirim,
                        readOnly: true,
                        decoration: const InputDecoration(
                          border: InputBorder.none,
                          suffixIcon: Icon(
                            Icons.calendar_month,
                          ),
                          hintText: 'dd-mm-yyyy',
                        ),
                        onTap: () async {
                          DateTime? pickedDate = await showDatePicker(
                              context: context,
                              initialDate: DateTime.now(),
                              firstDate: DateTime(2000),
                              lastDate: DateTime(2101));

                          if (pickedDate != null) {
                            // print(pickedDate);
                            String formattedDate =
                                DateFormat("yyyy-MM-dd").format(pickedDate);

                            setState(() {
                              var m = DateFormat('MM').format(pickedDate);
                              var d = DateFormat('dd')
                                  .format(pickedDate)
                                  .toString();
                              var Y = DateFormat('yyyy')
                                  .format(pickedDate)
                                  .toString();
                              var month = "";
                              switch (m) {
                                case '01':
                                  {
                                    month = "Januari";
                                  }
                                  break;
                                case '02':
                                  {
                                    month = "Februari";
                                  }
                                  break;
                                case '03':
                                  {
                                    month = "Maret";
                                  }
                                  break;
                                case '04':
                                  {
                                    month = "April";
                                  }
                                  break;
                                case '05':
                                  {
                                    month = "Mei";
                                  }
                                  break;
                                case '06':
                                  {
                                    month = "Juni";
                                  }
                                  break;
                                case '07':
                                  {
                                    month = "Juli";
                                  }
                                  break;
                                case '08':
                                  {
                                    month = "Agustus";
                                  }
                                  break;
                                case '09':
                                  {
                                    month = "September";
                                  }
                                  break;
                                case '10':
                                  {
                                    month = "Oktober";
                                  }
                                  break;
                                case '11':
                                  {
                                    month = "November";
                                  }
                                  break;
                                case '12':
                                  {
                                    month = "Desember";
                                  }
                                  break;
                              }
                              tanggalsuratpengirim.text = "$d $month $Y";
                            });
                          } else {
                            print("Date is not selected");
                          }
                        },
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Wajib Di isi';
                          }
                          return null;
                        },
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  const Text(
                    "Nama",
                    style: TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.w600,
                      fontFamily: 'Poppins',
                      color: Color(0xff4B556B),
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Container(
                    decoration: BoxDecoration(
                        border:
                            Border.all(color: Color(0xff276aa5), width: 1.0),
                        borderRadius: BorderRadius.circular(10)),
                    child: Padding(
                      padding: const EdgeInsets.only(left: 10),
                      child: TextFormField(
                        controller: nama,
                        decoration: const InputDecoration(
                          border: InputBorder.none,
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  const Text(
                    "Prodi",
                    style: TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.w600,
                      fontFamily: 'Poppins',
                      color: Color(0xff4B556B),
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Container(
                    decoration: BoxDecoration(
                        border:
                            Border.all(color: Color(0xff276aa5), width: 1.0),
                        borderRadius: BorderRadius.circular(10)),
                    child: Padding(
                      padding: const EdgeInsets.only(left: 10),
                      child: TextFormField(
                        controller: prodi,
                        decoration: const InputDecoration(
                          border: InputBorder.none,
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  const Text(
                    "NIM",
                    style: TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.w600,
                      fontFamily: 'Poppins',
                      color: Color(0xff4B556B),
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Container(
                    decoration: BoxDecoration(
                        border:
                            Border.all(color: Color(0xff276aa5), width: 1.0),
                        borderRadius: BorderRadius.circular(10)),
                    child: Padding(
                      padding: const EdgeInsets.only(left: 10),
                      child: TextFormField(
                        controller: nim,
                        decoration: const InputDecoration(
                          border: InputBorder.none,
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  const Text(
                    "Judul",
                    style: TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.w600,
                      fontFamily: 'Poppins',
                      color: Color(0xff4B556B),
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Container(
                    decoration: BoxDecoration(
                        border:
                            Border.all(color: Color(0xff276aa5), width: 1.0),
                        borderRadius: BorderRadius.circular(10)),
                    child: Padding(
                      padding: const EdgeInsets.only(left: 10),
                      child: TextFormField(
                        controller: judul,
                        decoration: const InputDecoration(
                          border: InputBorder.none,
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  const Text(
                    "Tempat",
                    style: TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.w600,
                      fontFamily: 'Poppins',
                      color: Color(0xff4B556B),
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Container(
                    decoration: BoxDecoration(
                        border:
                            Border.all(color: Color(0xff276aa5), width: 1.0),
                        borderRadius: BorderRadius.circular(10)),
                    child: Padding(
                      padding: const EdgeInsets.only(left: 10),
                      child: TextFormField(
                        controller: tempat,
                        decoration: const InputDecoration(
                          border: InputBorder.none,
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  const Text(
                    "Tanggal",
                    style: TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.w600,
                      fontFamily: 'Poppins',
                      color: Color(0xff4B556B),
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Container(
                    height: 50,
                    decoration: BoxDecoration(
                        border:
                            Border.all(color: Color(0xff276aa5), width: 1.0),
                        borderRadius: BorderRadius.circular(10)),
                    child: Padding(
                      padding: const EdgeInsets.only(left: 10),
                      child: TextFormField(
                        controller: tanggal,
                        readOnly: true,
                        decoration: const InputDecoration(
                          border: InputBorder.none,
                          suffixIcon: Icon(
                            Icons.calendar_month,
                          ),
                          hintText: 'dd-mm-yyyy',
                        ),
                        onTap: () async {
                          DateTime? pickedDate = await showDatePicker(
                              context: context,
                              initialDate: DateTime.now(),
                              firstDate: DateTime(2000),
                              lastDate: DateTime(2101));

                          if (pickedDate != null) {
                            // print(pickedDate);
                            String formattedDate =
                                DateFormat("yyyy-MM-dd").format(pickedDate);

                            setState(() {
                              var m = DateFormat('MM').format(pickedDate);
                              var d = DateFormat('dd')
                                  .format(pickedDate)
                                  .toString();
                              var Y = DateFormat('yyyy')
                                  .format(pickedDate)
                                  .toString();
                              var month = "";
                              switch (m) {
                                case '01':
                                  {
                                    month = "Januari";
                                  }
                                  break;
                                case '02':
                                  {
                                    month = "Februari";
                                  }
                                  break;
                                case '03':
                                  {
                                    month = "Maret";
                                  }
                                  break;
                                case '04':
                                  {
                                    month = "April";
                                  }
                                  break;
                                case '05':
                                  {
                                    month = "Mei";
                                  }
                                  break;
                                case '06':
                                  {
                                    month = "Juni";
                                  }
                                  break;
                                case '07':
                                  {
                                    month = "Juli";
                                  }
                                  break;
                                case '08':
                                  {
                                    month = "Agustus";
                                  }
                                  break;
                                case '09':
                                  {
                                    month = "September";
                                  }
                                  break;
                                case '10':
                                  {
                                    month = "Oktober";
                                  }
                                  break;
                                case '11':
                                  {
                                    month = "November";
                                  }
                                  break;
                                case '12':
                                  {
                                    month = "Desember";
                                  }
                                  break;
                              }
                              tanggal.text = "$d $month $Y";
                            });
                          } else {
                            print("Date is not selected");
                          }
                        },
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Wajib Di isi';
                          }
                          return null;
                        },
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  const Text(
                    "Tembusan",
                    style: TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.w600,
                      fontFamily: 'Poppins',
                      color: Color(0xff4B556B),
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Container(
                    decoration: BoxDecoration(
                        border:
                            Border.all(color: Color(0xff276aa5), width: 1.0),
                        borderRadius: BorderRadius.circular(10)),
                    child: Padding(
                      padding: const EdgeInsets.only(left: 10),
                      child: TextFormField(
                        controller: tembusan,
                        decoration: const InputDecoration(
                          border: InputBorder.none,
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                ],
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            GestureDetector(
              onTap: () {
                showDialog(
                    context: context,
                    builder: (context) {
                      return AlertDialog(
                        actions: [
                          Container(
                            // width: MediaQuery.of(context).size.width,
                            child: Column(
                              children: [
                                const SizedBox(
                                  height: 20,
                                ),
                                const Padding(
                                  padding: EdgeInsets.all(15.0),
                                  child: Center(
                                    child: Text(
                                      "Apakah Data Yang Anda Masukkan Sudah Benar ?",
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                        fontSize: 18,
                                        fontWeight: FontWeight.w600,
                                        fontFamily: 'Poppins',
                                        color: Color(0xff4B556B),
                                      ),
                                    ),
                                  ),
                                ),
                                const SizedBox(
                                  height: 10,
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Row(
                                    children: [
                                      Expanded(
                                        child: GestureDetector(
                                          onTap: () {
                                            Navigator.pop(context);
                                          },
                                          child: Container(
                                            height: 40,
                                            decoration: BoxDecoration(
                                                color: Colors.blueAccent,
                                                borderRadius:
                                                    BorderRadius.circular(10)),
                                            child: const Center(
                                              child: Text(
                                                "Belum",
                                                textAlign: TextAlign.center,
                                                style: TextStyle(
                                                  fontSize: 16,
                                                  color: Colors.white,
                                                  fontFamily: 'Poppins',
                                                  fontWeight: FontWeight.w600,
                                                  letterSpacing: 1,
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                      const SizedBox(
                                        width: 15,
                                      ),
                                      Expanded(
                                        child: GestureDetector(
                                          onTap: () {
                                            listSuratSkp.add(SuratSkpModel(
                                                tanggalSurat: tanggalSurat.text,
                                                kepada: kepada.text,
                                                nomorsuratpengirim:
                                                    nomorsuratpengirim.text,
                                                tanggalsuratpengirim:
                                                    tanggalsuratpengirim.text,
                                                nama: nama.text,
                                                prodi: prodi.text,
                                                nim: nim.text,
                                                judul: judul.text,
                                                tempat: tempat.text,
                                                tanggal: tanggal.text,
                                                tembusan: tembusan.text));

                                            saveDataToStorage();

                                            Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        PdfView2(
                                                          tanggalSurat:
                                                              tanggalSurat.text,
                                                          kepada: kepada.text,
                                                          nomorsuratpengirim:
                                                              nomorsuratpengirim
                                                                  .text,
                                                          tanggalsuratpengirim:
                                                              tanggalsuratpengirim
                                                                  .text,
                                                          nama: nama.text,
                                                          prodi: prodi.text,
                                                          nim: nim.text,
                                                          judul: judul.text,
                                                          tempat: tempat.text,
                                                          tanggal: tanggal.text,
                                                          tembusan:
                                                              tembusan.text,
                                                          jabatan:
                                                              widget.jabatan,
                                                          namaterang:
                                                              widget.namaterang,
                                                          nip: widget.nip,
                                                          nomorsurat1: widget
                                                              .nomorsurat1,
                                                          nomorsurat2: widget
                                                              .nomorsurat2,
                                                          yangberttd:
                                                              widget.yangberttd,
                                                        )));
                                          },
                                          child: Container(
                                            height: 40,
                                            decoration: BoxDecoration(
                                                color: Colors.red,
                                                borderRadius:
                                                    BorderRadius.circular(10)),
                                            child: const Center(
                                              child: Text(
                                                "Lanjutkan",
                                                textAlign: TextAlign.center,
                                                style: TextStyle(
                                                  fontSize: 16,
                                                  color: Colors.white,
                                                  fontFamily: 'Poppins',
                                                  fontWeight: FontWeight.w600,
                                                  letterSpacing: 1,
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                const SizedBox(
                                  height: 20,
                                ),
                              ],
                            ),
                          ),
                        ],
                      );
                    });
              },
              child: Container(
                width: MediaQuery.of(context).size.width,
                height: 45,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(12),
                  color: Color(0xff276aa5),
                ),
                child: const Center(
                    child: Text(
                  "Simpan",
                  style: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.bold,
                      fontFamily: 'Poppins',
                      color: Colors.white,
                      letterSpacing: 1),
                )),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
