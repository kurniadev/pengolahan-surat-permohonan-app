import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

import '../../model/surat_skp_model.dart';
import '../pdfexport/pdfView2.dart';

class DataSKP extends StatefulWidget {
  const DataSKP(
      {super.key,
      required this.nomorsurat1,
      required this.nomorsurat2,
      required this.yangberttd,
      required this.namaterang,
      required this.jabatan,
      required this.nip});

  final String nomorsurat1, nomorsurat2, yangberttd, namaterang, jabatan, nip;

  @override
  State<DataSKP> createState() => _DataSKPState();
}

class _DataSKPState extends State<DataSKP> {
  final storage = const FlutterSecureStorage();

  final List<SuratSkpModel> _listSuratSkp = [];

  _getDataFromStorage() async {
    String? data = await storage.read(key: 'list-surat-skp');
    if (data != null) {
      final dataDecoded = jsonDecode(data);
      if (dataDecoded is List) {
        setState(() {
          _listSuratSkp.clear();
          for (var item in dataDecoded) {
            _listSuratSkp.add(SuratSkpModel.fromJson(item));
          }
        });
      }
    }
  }

  AndroidOptions _getAndroidOptions() => const AndroidOptions(
        encryptedSharedPreferences: true,
      );

  Future<void> deleteSecureData(SuratSkpModel item) async {
    await storage.delete(
      key: item.nama,
      // aOptions: _getAndroidOptions(),
    );
  }

  @override
  initState() {
    super.initState();
    _getDataFromStorage();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text(
            "Surat Keterangan Persetujuan",
            style: TextStyle(
              fontSize: 18,
              color: Colors.white,
              fontFamily: 'Poppins',
              fontWeight: FontWeight.w500,
              letterSpacing: 1,
            ),
          ),
          elevation: 0,
          backgroundColor: const Color(0xff276aa5),
          iconTheme: const IconThemeData(color: Colors.white),
        ),
        body: _listSuratSkp.isEmpty
            ? Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Center(
                    child: SizedBox(
                        height: 200,
                        child: Image.asset('assets/data-kosong.png')),
                  ),
                  const Text(
                    'Data Kosong',
                    style: TextStyle(
                      fontSize: 16,
                      color: Colors.black87,
                      fontFamily: 'Poppins',
                      fontWeight: FontWeight.w500,
                      letterSpacing: 1,
                    ),
                  ),
                ],
              )
            : ListView.separated(
                itemCount: _listSuratSkp.length,
                shrinkWrap: true,
                reverse: true,
                itemBuilder: (context, index) {
                  final item = _listSuratSkp[index];
                  return Container(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 5,
                      vertical: 5,
                    ),
                    child: InkWell(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => PdfView2(
                                      tanggalSurat: item.tanggalSurat,
                                      kepada: item.kepada,
                                      nomorsuratpengirim:
                                          item.nomorsuratpengirim,
                                      tanggalsuratpengirim:
                                          item.tanggalsuratpengirim,
                                      nama: item.nama,
                                      prodi: item.prodi,
                                      nim: item.nim,
                                      judul: item.judul,
                                      tempat: item.tempat,
                                      tanggal: item.tanggal,
                                      tembusan: item.tembusan,
                                      jabatan: widget.jabatan,
                                      namaterang: widget.namaterang,
                                      nip: widget.nip,
                                      nomorsurat1: widget.nomorsurat1,
                                      nomorsurat2: widget.nomorsurat2,
                                      yangberttd: widget.yangberttd,
                                    )));
                      },
                      child: Card(
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const SizedBox(
                                height: 10,
                              ),
                              Text(
                                '${item.nama} (${item.nim})',
                                style: const TextStyle(
                                  fontSize: 16,
                                  color: Colors.black87,
                                  fontFamily: 'Poppins',
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                              Text(
                                '${item.prodi}  ',
                                style: const TextStyle(
                                  fontSize: 12,
                                  color: Colors.grey,
                                ),
                              ),
                              const SizedBox(
                                height: 10,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  );
                },
                separatorBuilder: (context, index) {
                  return const Divider();
                },
              ));
  }
}
