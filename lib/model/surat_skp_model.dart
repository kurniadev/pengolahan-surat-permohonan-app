class SuratSkpModel {
  final String tanggalSurat;
  final String kepada;
  final String nomorsuratpengirim;
  final String tanggalsuratpengirim;
  final String nama;
  final String prodi;
  final String nim;
  final String judul;
  final String tempat;
  final String tanggal;
  final String tembusan;

  SuratSkpModel({
    required this.tanggalSurat,
    required this.kepada,
    required this.nomorsuratpengirim,
    required this.tanggalsuratpengirim,
    required this.nama,
    required this.prodi,
    required this.nim,
    required this.judul,
    required this.tempat,
    required this.tanggal,
    required this.tembusan,
  });

  factory SuratSkpModel.fromJson(Map<String, dynamic> json) {
    return SuratSkpModel(
      tanggalSurat: json['tanggalSurat'],
      kepada: json['kepada'],
      nomorsuratpengirim: json['nomorsuratpengirim'],
      tanggalsuratpengirim: json['tanggalsuratpengirim'],
      nama: json['nama'],
      prodi: json['prodi'],
      nim: json['nim'],
      judul: json['judul'],
      tempat: json['tempat'],
      tanggal: json['tanggal'],
      tembusan: json['tembusan'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'tanggalSurat': tanggalSurat,
      'kepada': kepada,
      'nomorsuratpengirim': nomorsuratpengirim,
      'tanggalsuratpengirim': tanggalsuratpengirim,
      'nama': nama,
      'prodi': prodi,
      'nim': nim,
      'judul': judul,
      'tempat': tempat,
      'tanggal': tanggal,
      'tembusan': tembusan,
    };
  }
}

class SuratSpiModel {
  final String tanggalSurat;
  final String kepada;
  final String nomorsuratpengirim;
  final String tanggalsuratpengirim;
  final String universitas;
  final String prodi;
  final String fakultas;
  final String tanggalkeperluan;
  final String tanggalkeperluan2;
  final String tembusan;

  SuratSpiModel({
    required this.tanggalSurat,
    required this.kepada,
    required this.nomorsuratpengirim,
    required this.tanggalsuratpengirim,
    required this.universitas,
    required this.prodi,
    required this.fakultas,
    required this.tanggalkeperluan,
    required this.tanggalkeperluan2,
    required this.tembusan,
  });

  factory SuratSpiModel.fromJson(Map<String, dynamic> json) {
    return SuratSpiModel(
      tanggalSurat: json['tanggalSurat'],
      kepada: json['kepada'],
      nomorsuratpengirim: json['nomorsuratpengirim'],
      tanggalsuratpengirim: json['tanggalsuratpengirim'],
      universitas: json['universitas'],
      prodi: json['prodi'],
      fakultas: json['fakultas'],
      tanggalkeperluan: json['tanggalkeperluan'],
      tanggalkeperluan2: json['tanggalkeperluan2'],
      tembusan: json['tembusan'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'tanggalSurat': tanggalSurat,
      'kepada': kepada,
      'nomorsuratpengirim': nomorsuratpengirim,
      'tanggalsuratpengirim': tanggalsuratpengirim,
      'universitas': universitas,
      'prodi': prodi,
      'fakultas': fakultas,
      'tanggalkeperluan': tanggalkeperluan,
      'tanggalkeperluan2': tanggalkeperluan2,
      'tembusan': tembusan,
    };
  }
}
